# CSS-Preprocessors



## Getting started

This is week 6 homework for  Using SASS with BEM Methodology.

### INFO

In CSS folder you can see four different SCSS files: variables (for importing variables like color and etc.), mobile (for mobile version), animation(code for background animations) and style. In **style.scss** we import all three files and add main styling. I used BEM methodology in naming class so it could be easier to analyse css-html releationship. By simple running **index.html** file will appear with no styling and you will need live SASS compiler to run this project. 